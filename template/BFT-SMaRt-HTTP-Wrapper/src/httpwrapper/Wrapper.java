/**
 * 
 */
package httpwrapper;

import http.request.HTTPRequest;
import http.request.PUTRequest;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Christoph Englich
 *
 */
public class Wrapper {

	public static HTTPRequest wrap(byte[] content, String host, boolean incremented) {
		PUTRequest p = new PUTRequest(content);
		p.setHeader("Host", host);
		if (incremented)
			p.setHeader("Incremented", "1");
		return p;
	}
	
	public static byte[] unwrap(InputStream in) throws IOException {
		HTTPRequest r = new HTTPRequest(in);
		return r.getContent();
	}
	
	public static HTTPRequest unwrapToHTTPRequest(InputStream in) throws IOException {
		return new HTTPRequest(in);
	}
}
