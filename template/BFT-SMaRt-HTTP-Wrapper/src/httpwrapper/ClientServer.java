/**
 * 
 */
package httpwrapper;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author christoph
 *
 */
public class ClientServer extends Server {

	public ClientServer(Socket in, Socket out, Forwarder forwarder) {
		super(in, out, forwarder);
		new Server(in, out, new SimpleForwarder());
	}
	
	public void run() {
		while(run) {
			try {
				if(in.getInputStream().available() > 0) {
					System.out.println("ForwardServer: Forwarding...");
					forwarder.forward(in, out);
				} else {
//					if (counter == 1000) {
//						System.out.println("ForwardServer: Nothing available, sleep...");
//						counter = 0;
//					} else {
//						counter++;
//					}
					Thread.sleep(20);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			in.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
