/**
 * 
 */
package httpwrapper;

import java.io.IOException;
import java.net.Socket;

/**
 * @author Christoph Englich
 *
 */
public class UnwrapForwarder implements Forwarder {
	
	public void forward(Socket in, Socket out) throws IOException {
		out.getOutputStream().write(Wrapper.unwrap(in.getInputStream()));
	}
}
