/**
 * 
 */
package httpwrapper;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author Christoph Englich
 *
 */
public class IncomingServer extends Thread {

	private int port, bftport;
	private boolean running = true;
	
	public IncomingServer(int port, int bftport) {
		this.port = port;
		this.bftport = bftport;
	}
	
	public void run() {
		try {
			ServerSocket s = new ServerSocket(port);
			while(running) {
				Socket sin = s.accept();
				Socket out = new Socket("localhost", bftport);
				new SortingServer(sin, out, new Socket("localhost", bftport + 1), new SortForwarder());
			    new Server(out, sin, new SimpleForwarder());
			}
			s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void shutdown() {
		running = false;
	}
}
