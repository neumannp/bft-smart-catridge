/**
 * 
 */
package httpwrapper;

import java.io.IOException;
import java.net.Socket;

/**
 * @author Christoph Englich
 *
 */
public class SimpleForwarder implements Forwarder {

	/* (non-Javadoc)
	 * @see httpwrapper.Forwarder#forward(java.net.Socket, java.net.Socket)
	 */
	@Override
	public void forward(Socket in, Socket out) throws IOException {
		int a = in.getInputStream().available();
		while(a > 0) {
			byte[] buffer = new byte[a];
			in.getInputStream().read(buffer);
			out.getOutputStream().write(buffer);
			a = in.getInputStream().available();
		}
	}

}
