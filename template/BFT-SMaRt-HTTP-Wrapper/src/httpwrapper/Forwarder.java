/**
 * 
 */
package httpwrapper;

import java.io.IOException;
import java.net.Socket;

/**
 * @author Christoph Englich
 *
 */
public interface Forwarder {
	
	public void forward(Socket in, Socket out) throws IOException;
}
