/**
 * 
 */
package httpwrapper;

import java.io.IOException;
import java.net.Socket;

/**
 * @author Christoph Englich
 *
 */
public class SortingServer extends Thread {

	public Socket in, out, out2;
	public SortForwarder forwarder;
	private boolean run = true;
	
	public SortingServer(Socket in, Socket out, Socket out2, SortForwarder forwarder) {
		this.in = in;
		this.out = out;
		this.out2 = out2;
		this.forwarder = forwarder;
	}
	
	public void run() {
		int counter = 0;
		while(run) {
			try {
				if(in.getInputStream().available() > 0) {
					System.out.println("ForwardServer: Forwarding...");
					forwarder.forward(in, out, out2);
				} else {
//					if (counter == 1000) {
//						System.out.println("ForwardServer: Nothing available, sleep...");
//						counter = 0;
//					} else {
//						counter++;
//					}
					Thread.sleep(20);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			in.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void shutdown() {
		run =  false;
	}
}
