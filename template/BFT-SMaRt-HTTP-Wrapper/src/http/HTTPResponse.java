/**
 * 
 */
package http;

/**
 * @author christoph
 *
 */
public interface HTTPResponse {

	/**
	 * Checks whether the operation is working and returns the needed header
	 * 
	 * @return header The HTTP Header
	 */
	public String header();
	
	/**
	 * If the response has some content, it can be fetched here
	 * 
	 * @return content The response's content in bytes
	 */
	public byte[] content();
	
	/**
	 * Checks whether the repsonse has any content 
	 * 
	 * @return hasContent true if content to send is available, false if not
	 */
	public boolean hasContent();
}
