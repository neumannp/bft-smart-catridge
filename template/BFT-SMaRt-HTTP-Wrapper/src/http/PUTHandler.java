/**
 * 
 */
package http;

import http.request.HTTPRequest;


/**
 * @author Christoph Englich
 *
 */
public class PUTHandler implements HTTPResponse {

	private HTTPRequest req;
	private String docRoot;
	
	public PUTHandler(HTTPRequest req, String docRoot) {
		this.req = req;
		this.docRoot = docRoot;
	}
	
	/* (non-Javadoc)
	 * @see http.HTTPResponse#respond()
	 */
	@Override
	public String header() {
//		String path = this.docRoot + req.getPath();
//		try {
//
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return null;
	}
	
	public byte[] content() {
		return null;
	}
	
	public boolean hasContent() {
		return false;
	}

}
