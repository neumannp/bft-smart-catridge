/**
 * 
 */
package http;

import http.request.HTTPRequest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author Christoph Englich
 *
 */
public class GETHandler implements HTTPResponse {
	
	private HTTPRequest req;
	private String docRoot;
	private byte[] content;
	
	public GETHandler(HTTPRequest req, String docRoot) {
		this.req = req;
		this.docRoot = docRoot;
	}

	/* (non-Javadoc)
	 * @see http.HTTPResponse#getResponseText()
	 */
	@Override
	public String header() {
		File f = new File(docRoot + req.getPath());
		content = new byte[(int) f.length()];
		if (f.exists()) {
			try {
				FileInputStream in = new FileInputStream(f);
				in.read(content);
				return HTTP.SUCCESS("Connection: close\n", content);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return HTTP.ERROR404();
			} catch (IOException e) {
				e.printStackTrace();
				return HTTP.ERROR500();
			}
		} else {
			return HTTP.ERROR404();
		}
	}
	
	public byte[] content() {
		return content;
	}
	
	public boolean hasContent() {
		return content != null;
	}

}
