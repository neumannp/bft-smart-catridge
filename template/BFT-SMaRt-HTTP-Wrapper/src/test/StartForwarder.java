package test;

import httpwrapper.Forwarder;
import httpwrapper.IncomingServer;
import httpwrapper.SocketLauncher;
import httpwrapper.WrapForwarder;

import java.io.IOException;
import java.net.ServerSocket;

import config.HostConfig;

public class StartForwarder {

	public static void main(String[] args) {
		int outsidePort = 0;
		try {
			outsidePort = Integer.parseInt(args[0]);
		} catch (Exception e) {
			System.out.println("First argument needs to be the outside port! aborting...");
			System.exit(1);
		}
		HostConfig config = new HostConfig();
		int ownId = config.getLocalId();
		
		IncomingServer inServer = new IncomingServer(outsidePort, config.getHostPort(ownId));
		inServer.start();
		
		for (int i = 0; i < 4; i++) {
			if(i!=ownId){
				try {
					Forwarder fw = new WrapForwarder(config.getHostName(i), false);
					System.out.println(config.getHostPort(i));
					ServerSocket serverS = new ServerSocket(config.getHostPort(i));
					SocketLauncher socketL = new SocketLauncher(serverS, config.getHostName(i), 80, fw);
					socketL.start();
					
					//incremented
					Forwarder fw1 = new WrapForwarder(config.getHostName(i), true);
					ServerSocket serverS1 = new ServerSocket(config.getHostPort(i)+1);
					SocketLauncher socketL1 = new SocketLauncher(serverS1, config.getHostName(i), 80, fw1);
					socketL1.start();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

}
