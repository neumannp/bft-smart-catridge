/**
 * 
 */
package writer;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import reader.HostConfigReader;

/**
 * @author Frieder
 * 
 */
public class HostConfigWriter {

	public HostConfigWriter() {

	}

	/**
	 * 
	 * @param conf
	 *            The config that is written in ownHost.config
	 * @return the starting port of the specified id
	 */
	public int writeConfig(HostConfigReader conf, int id) {

		int portOfID = 0;

		String sep = System.getProperty("file.separator");

		String path = ".." + sep + "BFT-SMaRt" + sep + "config" + sep + "hosts.config";
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(path), "UTF-8"));

			int lastPort = 0;
			int newPort = 0;
			int portGap = 10;
			String line = 0 + " " + "localhost " + conf.getPort(0);
			lastPort = conf.getPort(0);
			if (id == 0)
				portOfID = lastPort;

			bw.write(line);
			for (int i = 1; i < conf.getNum(); i++) {
				bw.append(System.getProperty("line.separator")); // e.g. "\n"
				newPort = (conf.getPort(i) - conf.getPort(i - 1)) * portGap
						+ lastPort;
				line = i + " " + "localhost " + newPort;
				lastPort = newPort;
				if (id == i)
					portOfID = lastPort;
				bw.append(line);
			}
			bw.append(System.getProperty("line.separator")); // e.g. "\n"
		} catch (IOException e) {
			System.err.println("Konnte Datei nicht erstellen");
		} finally {
			if (bw != null)
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

		return portOfID;
	}
}
