package writer;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import reader.HostConfigReader;

public class ForwarderConfigWriter {

	public ForwarderConfigWriter() {

	}

	public boolean writeConfig(HostConfigReader conf, int id, String[] urls) {

		boolean succ = false;

		String sep = System.getProperty("file.separator");

		String path = ".." + sep + "BFT-SMaRt" + sep + "config" + sep + "hostname.config";
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(path), "UTF-8"));

			String line = "localhostid " + id;
			bw.write(line);
			bw.append(System.getProperty("line.separator")); // e.g. "\n"

			if (conf.getNum() == urls.length - 1) {
				//Using the Urls of the commandline
				for (int i = 0; i < conf.getNum(); i++) {
					line = i + " " + urls[i + 1];
					bw.append(line);
					bw.append(System.getProperty("line.separator"));
				}
			} else {
				//Using the Urls of the ownHost.config
				for (int i = 0; i < conf.getNum(); i++) {
					line = i + " " + conf.getHost(i);
					bw.append(line);
					bw.append(System.getProperty("line.separator"));
				}
				
			}
			succ = true;
		} catch (IOException e) {
			System.err.println("Konnte Datei nicht erstellen");
		} finally {
			if (bw != null)
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

		return succ;
	}
}
