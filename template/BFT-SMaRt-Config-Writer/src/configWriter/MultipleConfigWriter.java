/**
 * 
 */
package configWriter;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * @author christoph
 *
 */
public class MultipleConfigWriter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		HashMap<Integer, String> urls = new HashMap<Integer, String>();
		BufferedReader bin = new BufferedReader(new InputStreamReader(System.in));
		int startingPort = Integer.parseInt(args[0]);
		int replicas;
		try {
			System.out.println("Enter the number of replicas (4 = 0..3):");
			replicas = Integer.parseInt(bin.readLine());
			for(int i = 0; i < replicas; i++) {
				System.out.println("Enter URL (withouth http://) for replica " + i + ":");
				urls.put(i, bin.readLine());
			}
			for (int i = 0; i < replicas; i++) {
				FileWriter fw = new FileWriter(i + "hosts,config");
				FileWriter fw2 = new FileWriter(i + "proxy.config");
				for(int j = 0; j < replicas; j++) {
					fw.write(j + " localhost " + (startingPort + i * replicas * 2 + j * 2) + System.getProperty("line.separator"));
					fw2.write(j + " " + urls.get(j) + System.getProperty("line.separator"));
				}
				fw.close();
				fw2.close();
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
